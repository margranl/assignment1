<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
			try{
            $this->db = new PDO('mysql:host=localhost;dbname=testdb;charset=utf8mb4', 'username', 'password');
			} catch (PDOException $e){
				die('Sorry, database problem');
			} 
			echo '';
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
		try{
			$query = $this->db->query('SELECT * FROM book');
			while($row = $query-> fetch()){
					$booklist[] =new Book( $row['title'], $row['author'], $row['description'], $row['id']);
			}
		}catch (PDOException $e)
		{
			echo $e->getMessage();
		}
        return $booklist; 
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = null;
		if(is_numeric($id)){
		try 
		{
			$db=("SELECT * FROM book WHERE id = '$id'") ;
			$statement= $this->db->prepare($db);
			$statement->execute(array($id));
			if ($row = $statement->fetch(PDO::FETCH_ASSOC)){
				$book= new Book($row['title'], $row['author'], $row['description'], $row['id']);
			}
		} catch(PDOException $e)
			{
				echo $e->getMessage();
			}
        return $book;
		} else
			echo "Feil";
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		if( $book->title != '' && $book->author != '' ){
		if($book->description == ''){
			$book->description = NULL;
		}
		$db = "INSERT INTO book(title, author, description) VALUES (:title, :author, :description)";
		$query = $this->db->prepare($db);
		$query->execute(array(
			':title' => $book->title,
			':author' => $book->author,
			':description' => $book->description));
			$book->id = $this->db->lastInsertId();
		}else {
				echo "Both author and title must be filled out";
		}
		$book->id = $this->db->lastInsertId();
		
		// {$title},{$author},{$description}
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {

		if( $book->title != '' && $book->author != '' && $book->description != ''){
		$stmt = $this->db->prepare("UPDATE book SET title=?, author =?, description=? WHERE id ='$book->id'");
		$stmt->execute(array($book->title, $book->author, $book->description));
		} else {
			echo "Both author and title must be filled out";
		}
	}

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		$sql = "DELETE FROM book WHERE id = :id";
		$query = $this->db->prepare( $sql );
		echo "id = $id";
		$query->execute( array( ":id" => $id ) );
			
    }
	
}

?>